This repository contains my professional resume and some supporting data (now outdated) that I've previously used in job searches.

# Resume Build

The folder "res" contains my resume formatted in [LaTeX](https://www.latex-project.org/about/).

The file [res.cls](./res/res.cls) defines the resume template. According to the header an author citations there, it's originally from Michael DeCorte in 1989 and was updated for the 2e version of LaTeX in 2001 by Venkat Krishnamurthy.

My resume in [resume.tex](./res/resume.tex) uses the constructs of the template for typesetting.

I use a Gitlab CI/CD pipeline defined in [.gitlab-ci.yml](./.gitlab-ci.yml) to compile the resume. The one job in the CI pipeline runs in a Docker container
which uses a [public base image](https://hub.docker.com/r/raabf/latex-versions/) with LaTeX tools and fonts installed.

The resulting PDF of my resume is currently archived for each CI/CD pipeline execution here on Gitlab.com. You can find a recent
build [here](https://gitlab.com/bryantrobbins/jobsearch/-/jobs/5247658397/artifacts/file/res/resume.pdf). Someday, I may try to deploy
this somewhere more accessible on the Web  ... but if I'm going to deploy a resume, I might want to build a Website too!